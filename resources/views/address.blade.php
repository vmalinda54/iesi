<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkout | Address</title>
</head>
<body>
    <h1>Halaman Address</h1>
    <h2>{{ $product->name }}</h2>
    <p>{{ $product->description }}</p>
    <p>{{ $product->stock }}</p>
    <p>{{ $product->price }}</p>
    <form action="/payment" method="post">
        @csrf
        <input type="text" name="name" placeholder="name"><br>
        <input type="text" name="address" placeholder="address"><br>
        <input type="text" name="contact_number" placeholder="contact_number"><br>
        <input type="text" name="city" placeholder="city"><br>
        <input type="text" name="province" placeholder="province"><br>
        <input type="submit" value="pay">
    </form>
</body>
</html>