<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Product Details</title>
</head>
<body>
    <h1>Halaman Product</h1>
    <h2>{{ $product->name }}</h2>
    <p>{{ $product->description }}</p>
    <p>{{ $product->stock }}</p>
    <p>{{ $product->price }}</p>
    <form action="/address" method="post">
        @csrf
        <input type="number" name="quantity" placeholder="quantity"> <br>
        <input type="submit" value="checkout">
    </form>
</body>
</html>