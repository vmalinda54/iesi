<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Checkout | Payment</title>
</head>
<body>
    <h1>Halaman Payment</h1>
    <h2>{{ $product->name }}</h2>
    <p>{{ $product->description }}</p>
    <p>{{ $product->stock }}</p>
    <p>{{ $product->price }}</p>
    <form action="/checkout" method="post">
        @csrf
        <input type="text" name="name" placeholder="name"><br>
        <input type="text" name="number" placeholder="number"><br>
        <input type="number" name="month" placeholder="month"><br>
        <input type="number" name="year" placeholder="year"><br>
        <input type="number" name="cvc" placeholder="cvc"><br>
        <input type="submit" value="pay">
    </form>
</body>
</html>