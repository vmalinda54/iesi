<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function address(Request $request)
    {
        $input = $request->input();
        $product = Product::find(session('product')['id']);
        $quantity = $input['quantity'];
        session(['product' => ['id' => $product->id, 'quantity' => $quantity ]]);
        return view('address', ['product' => $product]);
    }

    public function payment(Request $request)
    {
        $input = $request->input();
        $product = Product::find(session('product')['id']);
        return view('payment', ['product' => $product]);
    }

    public function checkout(Request $request)
    {
        $input = $request->input();
        $product = Product::find(session('product')['id']);
        $product->stock -= session('product')['quantity'];
        $product->update();
        return $this->redirect();
    }

    public function redirect(){
        return redirect()->to('/');
    }
}
